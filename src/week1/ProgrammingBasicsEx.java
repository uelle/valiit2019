package week1;

import java.util.Scanner;



public class ProgrammingBasicsEx {
    public static void main(String[] args) {
//
//        // Ülesanne 2
//
//        String city = "Berlin";
//        if (city.equals("Milano")) {
//            System.out.println("Ilm on soe.");
//        } else {
//            System.out.println("Ilm polegi kõige tähtsam!");
//        }

// Ülesanne 3

//        int grade = 5;
//        if (grade == 1) {
//            System.out.println("Nõrk");
//        } else if (grade == 2){
//            System.out.println("Mitterahuldav");
//        } else if (grade == 3) {
//            System.out.println("Rahuldav");
//        } else if (grade == 4) {
//            System.out.println("Hea");
//        } else  if (grade == 5) {
//            System.out.println("Väga hea");
//        } else {
//            System.out.println("VIGA: Ebakorrektne hinne!");
//        }
//
//    }
//}
// Ülesanne 4
//
//        int age = 77;
//        if (age < 100) {
//            System.out.println("Noor");
//        } else {
//            System.out.println("Vana");
//        }
//    }
////}
////// Ülesanne 5
////
////int age = 30;
////if (age < 100) {
////    System.out.println("Noor");
////} else if (age == 100){
////    System.out.println("Peaaegu vana");
////} else if (age > 100){
////    System.out.println("Vana");
////} else {
////    System.out.println("VIGA - nii vanaks inimesed ei ela");
////}
////
////        }
////        }
////// Ülesanne 6
////textToPrint = (age  < 100) ? ("Noor") : ((age == 100) ? ("Peaaegu vana") : ("Vana"));
////System.out.println(textToPrint);
////
//
//
//// Ülesanne 7
//
//public class ProgrammingBasicsEx {
//    public static void main(String[] args) {
//
////        int[] myIntArr;  // Defineeri muutuja, mille tüübiks täisarv???
////       myIntArr = new int[5];
////       myIntArr[0] = 1;
////        myIntArr[1] = 2;
////        myIntArr[2] = 3;
////        myIntArr[3] = 4;
////        myIntArr[4] = 5;
////        System.out.println(String.format("Massiivi esimese elemendi väärtus: %s", myIntArr[0]));
////        System.out.println(String.format("Massiivi kolmanda elemendi väärtus: %s", myIntArr[2]));
////        System.out.println(String.format("Massiivi viimase elemendi väärtus: %s", myIntArr[myIntArr.length -1]));
//
//
//// Ülesanne 8
//
//        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Paris"};
//
//        // Ülesanne 9
////Variant 1 (lihtsam)
//        int[][] myTwoDimensionalArray = {
//                {1, 2, 3},
//                {4, 5, 6},
//                {7, 8, 9, 0}
//        };
//
//        // Variant 2 (pikem)
//
//        int[][] myTwoDimensionalArray2 = new int[3][];
//        myTwoDimensionalArray2[0] = new int[3];
//        myTwoDimensionalArray2[0][0] = 1;
//        myTwoDimensionalArray2[0][1] = 2;
//        myTwoDimensionalArray2[0][2] = 3;
//        myTwoDimensionalArray2[1] = new int[3];
//        myTwoDimensionalArray2[1][0] = 4;
//        myTwoDimensionalArray2[1][1] = 5;
//        myTwoDimensionalArray2[1][2] = 6;
//        myTwoDimensionalArray2[2] = new int[4];
//        myTwoDimensionalArray2[2][0] = 7;
//        myTwoDimensionalArray2[2][1] = 8;
//        myTwoDimensionalArray2[2][2] = 9;
//        myTwoDimensionalArray2[2][3] = 0;
//
//        // Ülesanne 10
//
//        // Variant 1
//
//        String[][] countryCities = new String[3][];
//        countryCities[0] = new String[]{"Tallinn", "Tartu", "Valga", "Võru"};
//        countryCities[1] = new String[]{"Stockholm", "Uppsala", "Lund", "Köping"};
//        countryCities[2] = new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"};
//
//        // Variant 2
//
//        String[][] countryCities2 = {
//                {"Tallinn", "Tartu", "Valga", "Võru"},
//                {"Stockholm", "Uppsala", "Lund", "Köping"},
//                {"Helsinki", "Espoo", "Hanko", "Jämsä"}
//        };
//
        // Ülesanne 11

//        int numberToPrint = 1;
//        while (numberToPrint <= 0) {
//            System.out.println(numberToPrint++);
//        }
//
//        // Ülesanne 12
//
//
//        // Ülesanne 13
//        // Variant 1
//
//        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//        for (int num : numbers) {
//            System.out.println(num);
//        }
//
//        {
//
//        }
//
//        // Variant 2
//        for (int num : new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}) {
//            System.out.println(num);
//        }
//
//        // Ülesanne 14
//
//        for (int i = 1; i <= 100; i++) {
//            if (i % 3 == 0) {
//                System.out.println(i);
//            }
//        }

        // Ülesanne 15

//        String[] bands = {"Sun", "Metsatöll", "Queen", "Metallica"};
//        String commaSeperatedValues = "";
//        for (int i = 0; i < bands.length; i++) {
//            commaSeperatedValues = commaSeperatedValues + bands[i];
//            if (i + 1 < bands.length) {
//                commaSeperatedValues = commaSeperatedValues + ", ";
//            }
//            System.out.println(commaSeperatedValues);
//        }

        // Ülesanne 16

//        String[] bands = {"Sun", "Metsatöll", "Queen", "Metallica"};
//        String commaSeperatedValues = "";
//        for (int i = bands.length - 1; i >= 0; i--) {
//            commaSeperatedValues = commaSeperatedValues + bands[i];
//            if (i > 0) {
//                commaSeperatedValues = commaSeperatedValues + ", ";
//            }
//        }
//        System.out.println(commaSeperatedValues);


        // Ülesanne 17
//
//        String[] numberWords = {"null", "üks", "kaks", "kolm", "neli",
//                "viis", "kuus", "seitse", "kaheksa", "üheksa"
//        };
//
//        if (args.length > 0) {
//            int index = 0;
//            for (String numberText : args) {
//                int numberValue = Integer.parseInt(numberText);
//                System.out.print(numberWords[numberValue]);
//                if (index < args.length - 1) {
//                    System.out.print(", ");
//                }
//                index++;
//            }
//
//        }
        // Scanneriga lahendus

//        Scanner digitScanner = new Scanner(System.in);
//        int[] digitsToPrint = new int[10_000];
//        int ii = 0;
//        for(; ii < digitsToPrint.length; ii++) {
//            System.out.println("Sisesta number vahemikus 0-9:");
//            String insertedValue = digitScanner.nextLine();
//            insertedValue = insertedValue.trim();
//            if (insertedValue.length() == 0) {
//                break;
//            } else {
//                digitsToPrint[ii] = Integer.parseInt(insertedValue);
//            }
//            if (ii == digitsToPrint.length - 1) {
//                System.out.println("Sorry, rohkem ei saa, käivita palun programm uuesti, kui soovid sisestada");
//            }
//        }

//        // Selleks hetkeks on meil vajalikud numbrid olemas!

//        for (int i = 0; i < ii; i++) {
//            int digitToPrint = digitsToPrint[i]; // Numbriline väärtus
//            System.out.println(numberWords[digitToPrint]);
//            if (i < ii) {
//                System.out.println(", ");
//            }
//        }


        // Igav Lahendus
//        Scanner numberReaderScanner = new Scanner(System.in);
//        while (true) {
//            System.out.println("Sisesta number vahemikus 0 - 9:");
//            String insertedNumber = numberReaderScanner.nextLine();
//            if (insertedNumber.length() == 0) { // Kas soovime programmi töö lõpetada?
//                break;
//            } else {
//                int insertedNumberDigit = Integer.parseInt(insertedNumber);
//                System.out.println(numberWords[insertedNumberDigit]);
//
//
//            }
//
//        }
//        Scanner numberReaderScanner = new Scanner(System.in);
//        while (true) {
//            System.out.println("Sisesta number vahemikus 0 - 9:");
//            int insertedNumber = numberReaderScanner.nextInt();
//            if (insertedNumber < 0) {
//                break;
//            }
//                switch (insertedNumber) {
//                    case 0:
//                        System.out.println("null");
//                        break;
//                    case 1:
//                        System.out.println("üks");
//                        break;
//                    case 2:
//                        System.out.println("kaks");
//                        break;
//                    default:
//                        System.out.println("VIGA: ebakorrektne number");
//
//
//            }
//        }

        // Ülesanne 18

        System.out.println();
        double randomNumber;
        do {
            System.out.println("Tere");
            randomNumber = Math.random();
        } while (randomNumber < 0.5);













    }

}



















