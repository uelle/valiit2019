package week2;

public interface Car { // defineeritakse ära, et tulevikus on vastavad klassid (interface´i kohta)
     void drive();
     void sppedUp();
     void stop ();
     void addDriver(Human driver);
     int getMaxSpeecd();
}
