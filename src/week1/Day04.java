package week1;

import java.sql.Array;
import java.util.*;
import java.util.ArrayList;
import java.util.List;

public class Day04 {
    public static void main(String[] args) {

        //-----------
        // List
        //----------
//        List<String> names = new ArrayList<>();
//        names.add("Thomas");
//        names.add("Mary");
//        names.add("Linda");
//        System.out.println(names);
//
//        int inserPosition = names.indexOf("Linda");
//        System.out.println(inserPosition);
//        names.add(2, "Ruth");
//        names.add(inserPosition, "Ruth");
//        System.out.println(names);
//
//        // Kas antud väärtus on juba listis olemas?
//        System.out.println(names.contains("Linda"));
//        System.out.println(names.contains("Joe"));

        // Asenda element indeksi kohal x...
//        int lindaPosition = names.indexOf("Linda");
//        if (lindaPosition >= 0) {
//            names.set(lindaPosition, "Laura");
//        }
//        System.out.println(names);
//
//        for (String name : names) {
//            System.out.println("Nimi: " + name);
//        }
//        System.out.println(names.get(3)); // Kindla indeksiga elemendi pärimine
//
//        // Mitmetasandiline List
//        List<List<String>> complexList = new ArrayList<>();
//
//        List<String> humans = Arrays.asList("Mark", "Mary", "Thomas", "Linda");
//        complexList.add(humans);
//
//        List<String> cars = Arrays.asList("BMW", "Reanult", "Opel", "Ford");
//        complexList.add(cars);
//
//        complexList.add(Arrays.asList("Tupolev", "Jakolev", "Boeing", "Airbus"));
//
//
//        System.out.println(complexList);
//        System.out.println(complexList.get(2).get(1));

        //----------
        // Set
        // ----------

        Set<String> uniqueNames = new HashSet<>();
        uniqueNames.add("Mary");
        uniqueNames.add("Thomas");
        uniqueNames.add("Mark");
        uniqueNames.add("Mary");

        System.out.println(uniqueNames);
        Object[] uniqueNamesArr = uniqueNames.toArray(); // Tagastab massiivi objektidest

        for (String uniqueName : uniqueNames) {
            System.out.println("Unikaalne nimi: " + uniqueName);

        }

        uniqueNames.remove("Mark");
        System.out.println(uniqueNames);


        // uniqueNames.size()

        Comparator<String> nameComparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        };

        // Tree Set
        Set<String> sortedNames = new TreeSet<>();
        sortedNames.add("Mary");
        sortedNames.add("Thomas");
        sortedNames.add("Mark");
        System.out.println(sortedNames);


        //------------
        // Map
        //Võtme ja väärtuse paaride kogum
        // Võti1 --> Väärtus1
        // Võti 2 --> Väärtus2
        // TreeX (TreeSet, TreeMap) - sorteerimise üle saad ise otsustada
        // HashX (HashSet, HashMap) - Java virtuaalmasin otsustab sinu eest järjekorra

        Map<String, String> phoneNumbers = new HashMap<>();
        phoneNumbers.put("Mati", "+37255 786 111");
        phoneNumbers.put("Kati", "+37255 786 333");
        phoneNumbers.put("Karin", "+37255 786 333");

        for (String key : phoneNumbers.keySet()) {
            System.out.println("Sõber: " + key + ", telefon: " + phoneNumbers.get(key));
        }
        phoneNumbers.put("Mari", "0045645745");
        phoneNumbers.put("Leonardo", "00043497");
        System.out.println(phoneNumbers);

        Map<String, Map<String, String>> contacts = new TreeMap<>();

        Map<String, String> mariContactDetails = new TreeMap<>();
        mariContactDetails.put("Phone", "54645742");
        mariContactDetails.put("Email", "mari@kari.mail.ee");
        mariContactDetails.put("Aadress", "Tamme 6a, Palgipõllu");
        contacts.put("Mari", mariContactDetails);


        Map<String, String> martContactDetails = new TreeMap<>();
        martContactDetails.put("Phone", "111145742");
        martContactDetails.put("Email", "mart@bcs.mail.ee");
        martContactDetails.put("Aadress", "Kuuse 6, Elva");
        contacts.put("Mart", martContactDetails);

        System.out.println(contacts);
        System.out.println("Mardi telefoninumber: " + contacts.get("Mart").get("Phone"));


        // Ülesanne 19 (Programming basics)

        List<String> cities = new ArrayList<>();
        cities.add("Tartu");
        cities.add("Tallinn");
        cities.add("Võru");
        cities.add("Narva");
        cities.add("Paldiski");

        System.out.println("Esimene element: " + cities.get(0));
        System.out.println("Kolmas element: " + cities.get(3));
        System.out.println("Viimane element: " + cities.get(cities.size() - 1));

        // Ülesanne 20 (Programming basics)
        // First In, First Out (FIFO)

        Queue<String> names = new LinkedList<>();
        names.add("Mary");
        names.add("Henry");
        names.add("Mark");
        names.add("Manfred");
        names.add("Ursula");
        names.add("Angela");
        while (!names.isEmpty()) {
            System.out.println("Tõmbasin queuest sellise nime: " + names.remove());

        }

        // Ülesanne 21 (Programming basics)

        Set<String> nameSet = new TreeSet<>();
        nameSet.add("Mary");
        nameSet.add("Henry");
        nameSet.add("Mark");
        nameSet.add("Manfred");
        nameSet.add("Ursula");
        nameSet.add("Angela");
        nameSet.forEach(myNameToPrint -> System.out.println("Lambda-avaldis prindib: " + myNameToPrint));

        // Ülesanne 22

        Map<String, String[]> countryCities = new TreeMap<>();
        countryCities.put("Estonia", new String[]{"Tallinn", "Tartu", "Valga", "Võru" });
        countryCities.put("Sweden", new String[]{"Stockholm", "Uppsala", "Lund", "Köping" });
        countryCities.put("Finland", new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä" });

        for (String country : countryCities.keySet()) {
            System.out.println("Country: " + country);
            String[] currentCountryCities = countryCities.get(country);
            for (String city : currentCountryCities) {
                System.out.println("\t" + city);
            }

        }
    }
}
