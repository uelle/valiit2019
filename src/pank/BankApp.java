package pank;


import java.io.IOException;
import java.util.Scanner;

public class BankApp {

    public static Scanner inputReader = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        AccountService.loadAccounts(args[0]); // see pole fail ise, vaid teksti kujul faili asukoht

        while (true) {
            System.out.println();
            System.out.println("Sisesta käsklus: ");
            String command = inputReader.nextLine();
            String[] commandParts = command.split(" ");
            switch (commandParts[0].toUpperCase()) {
                case "BALANCE":
                    displayAccountDetails(commandParts);
                    break;
                case "TRANSFER":
                    makeTransfer(commandParts);
                    break;
                case "EXIT":
                    return;
                default:
                    System.out.println("Tundmatu käsklus.");
            }
        }
    }


//            String accountNumber = inputReader.nextLine();
//            Account account = AccountService.searchAccount(accountNumber);
//            System.out.println("----KONTO----");
//            if (account != null) {
//                System.out.println("Name: " + accountToDisplay.getFirstName() + " " ? account.getLastName());
//                System.out.println("Number: " + accountToDisplay.getAccountNumber());
//                System.out.println("Balance: " + accountToDisplay.getBalance());
//            } else {
//                System.out.println("Kontot ei leitud");
//            }
//        }


    private static void displayAccountDetails(String[] commandParts) {
        if (commandParts.length == 2) {
            // Küsime kontonumbri järgi
            // BALANCE 85679485
            Account account1 = AccountService.searchAccount(commandParts[1]);
            displayAccountDetails(account1);


        } else if (commandParts.length == 3) {
            // Küsime eesnime ja perenime järgi
            // BALANCE Aadu Kaadu
            Account account2 = AccountService.searchAcount(commandParts[1], commandParts[2]);
            displayAccountDetails(account2);

        } else {
            System.out.println("Tundmatu käsklus");
        }
    }

    private static void displayAccountDetails(Account accountToDisplay) {
        System.out.println("----- KONTO -----");
        if (accountToDisplay != null) {
            System.out.println("Name: " + accountToDisplay.getFirstName() + " " + accountToDisplay.getLastName());
            System.out.println("Number: " + accountToDisplay.getAccountNumber());
            System.out.println("Balance: " + accountToDisplay.getBalance());
        } else {
            System.out.println("Kontot ei leitud");
        }

    }

    private static void makeTransfer(String[] commandParts) {
        if (commandParts.length == 4) {
            ActionResponse response = AccountService.transfer(commandParts[1], commandParts[2], Double.parseDouble(commandParts[3]));
            System.out.println(response.getMessage());
            if (response.isActionSuccessful()) {
                System.out.println("Maksja konto detailandmed");
                displayAccountDetails(response.getFromAccount());
                System.out.println("Saaja konto detailandmed");
                displayAccountDetails(response.getToAccount());
            }
        } else {
            System.out.println("Tundmatu käsklus.");
        }
    }
}
