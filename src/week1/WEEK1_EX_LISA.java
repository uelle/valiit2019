package week1;

public class WEEK1_EX_LISA {

        public static void main(String[] args) {

            // Exercise 1
            System.out.println("\nExercise 1:\n");

            // Pattern 1;
            for (int row = 1; row <= 8; row++) {
                for (int col = 1; col <= 19; col++) {
                    if (row % 2 == 1) {
                        System.out.print(col % 2 == 1 ? "#" : "+");
                    } else {
                        System.out.print(col % 2 == 1 ? "+" : "#");
                    }
                }
                System.out.println();
            }
            System.out.println();

            // Pattern 2
            for (int i = 1; i <= 6; i++) {
                for(int j = 1; j <= 6; j++) {
                    if (j == i) {
                        System.out.print("#");
                    } else if (j < i) {
                        System.out.print("-");
                    }
                }
                System.out.println();
            }
            System.out.println();

            // Pattern 3
            for (int row = 1; row <= 5; row++) {
                for (int col = 1; col <= 7; col++) {
                    if (row % 2 == 0) {
                        System.out.print("=");
                    } else {
                        System.out.print(col % 3 == 1 ? "#" : "+");
                    }
                }
                System.out.println();
            }
        }
    }

