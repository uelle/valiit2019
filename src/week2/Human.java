package week2;

public class Human { // vaikimisi kirjutatakse: "public class Human extends Object"
    public String name; // ei pea public ees olema --> Objektimuutuja. PAREM - name, age, weight -> PRIVATE
    public int age; // Objektimuutuja
    public double weight; // Objektimuutuja

    public static int humanCount = 0; // Klassimuutuja

    public Human(String name, int age, double weight) {
        this.name = name; // Objektimuutja
        this.age = age; // Objektimuutuja
        this.weight = weight; // Objektimuutuja

        // Objektimuutujaid võib olla palju - igal objektil oma koopia

        Human.humanCount++; // Klassimuutuja (ainult üks eksemplar)
    }

    public Human(String name) {

        this.name = name;
        Human.humanCount++;
    }


    public boolean isYoung() {
        return this.age < 100; // muutuja "age" on selle objekti oma (käib "this" kohta)

    }
    public static boolean isThisHumanYoung(Human human) {
        return human.age < 100;
    }


}



