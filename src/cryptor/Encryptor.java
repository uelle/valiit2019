package cryptor;

import java.util.List;

public class Encryptor extends Cryptor {

    // A--> Ü
    // B--> Ö

    @Override
    public void initAlphabet(List<String> alphabet) {
        for (String line : alphabet) {
            String[] lineParts = line.split(", ");
            this.cryptoMap.put(lineParts[0], lineParts[1]);
        }
    }
}

//// for-eachiga käib nii:
//        // for(char c : textChars) {
//        // String charString = String.valueOf(c)}
//        return convertedText;
//    }





