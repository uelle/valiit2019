package week2;

public abstract class Atleet { // Sellest klassist pole kunagi võimalik klassi luua ("abstract")
    public String firstName;
    public String lastName;
    public int age;
    public String gender;
    public int height;
    public double weight;

    public abstract void perform(); // Sellest klassist pole kunagi võimalik klassi luua ("abstract")



}

