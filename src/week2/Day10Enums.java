package week2;

public class Day10Enums {

    private Gender personGender;

    public Gender getPersonGender() {
        return personGender;
    }

    public static void main(String[] args) {
        Gender myGender = Gender.FEMALE;
        Gender myGender2 = Gender.MALE;
        Gender[] genders = Gender.values();

        if (myGender == myGender2) {
            System.out.println("On samast soost");
        } else {

            System.out.println("Ei ole samast soost");

        }
        Gender myGender3 = Gender.valueOf("MALE");
    }
}
