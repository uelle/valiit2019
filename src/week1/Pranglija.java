package week1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Pranglija {

    private static final int GUESS_COUNT = 3;

    public static void main(String[] args) {
        Scanner inputReader = new Scanner(System.in);

        List<String[]> log = new ArrayList<>();

        while (true) {
            System.out.println("Mäng algab...");
            System.out.println("Sisesta oma nimi:");
            String playerName = inputReader.nextLine();

            System.out.println("Sisesta genereeritavate arvude miinimumväärtus, mis suurem või võrdne 0:");
            int minValue;
            while (true) {
                minValue = inputReader.nextInt();
                if (minValue >= 0) {
                    break;
                }
                System.out.println("Vigane väärtus. Vali arv, mis on suurem või võrdne nulliga.");
            }

            System.out.println("Sisesta genereeritavate arvude maksimumväärtus:");
            int maxValue;
            while (true) {
                maxValue = inputReader.nextInt();
                if (maxValue > minValue) {
                    break;
                }
                System.out.println("VIGA! maksimumväärtus peab olema suurem, kui miinimumväärtus.");
            }
            int delta = maxValue - minValue;

            System.out.println();
            long startTime = System.currentTimeMillis();
            int i = 0;
            for (; i < GUESS_COUNT; i++) {
                int number1 = (int) (Math.random() * (delta + 1)) + minValue;
                int number2 = (int) (Math.random() * (delta + 1)) + minValue;
                int sum = number1 + number2;
                System.out.println(number1 + " + " + number2 + " = ");
                int guessedSum = inputReader.nextInt();
                if (guessedSum != sum) {
                    break;
                }
            }

            if (i != GUESS_COUNT) {
                System.out.println("Mäng läbi, sa kaotasid!");
            } else {
                long endTime = System.currentTimeMillis();
                long elapsedSeconds = (endTime - startTime) / 1000;
                System.out.println("Mäng läbi, sul kulus " + elapsedSeconds + " sekundit!");

                // Salvestame tulemuse logisse...
                String[] result = {playerName, String.valueOf(elapsedSeconds), minValue + ".." + maxValue};
                log.add(result);

                // Sorteerime tulemused aja järgi kasvavalt.
                Collections.sort(log, (result1, result2) -> {
                    int result1Time = Integer.parseInt(result1[1]); // 10
                    int result2Time = Integer.parseInt(result2[1]); // 11
                    return result1Time - result2Time;
                });

                // Kuvame tulemused...
                System.out.println("--------------------------------");
                System.out.println("TULEMUSED");
                System.out.println("--------------------------------");
                for (String[] resultItem : log) {
                    System.out.println("Nimi: " + resultItem[0]);
                    System.out.println("Aeg sekundites: " + resultItem[1]);
                    System.out.println("Vahemik: " + resultItem[2]);
                    System.out.println("--------------------------------");
                }
            }

            System.out.println("Mäng on lõppenud. Mida soovid edasi teha?");
            System.out.println("0 - välju mängust");
            System.out.println("1 - alusta uut mängu");
            int playerSelection;
            while (true) {
                playerSelection = inputReader.nextInt();
                if (playerSelection == 1 || playerSelection == 0) {
                    break;
                }
                System.out.println("Vigane valik, vali kas 0 või 1.");
            }

            if (playerSelection == 0) {
                break;
            }

            // Hack: muidu jääb viimane \n sisse lugemata ja nime sisestamine uue mängu alduses feilib.
            inputReader.nextLine();
        }
    }
}

