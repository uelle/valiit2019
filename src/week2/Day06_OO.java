package week2;

public class Day06_OO {
    public static void main(String[] args) {
//        Human rein = new Human(); // Rein on viide Human ojektile
//        rein.name = "Rein";
//        rein.age = 45;
//        rein.weight = 87;

        Human rein = new Human("Rein", 45, 87);

//        Human mari = new Human();
//        mari.name = "Mari";
//        mari.age = 28;
//        mari.weight = 55;
        Human mari = new Human("Mari", 28, 55);

        Human toomas = new Human("Toomas");

        System.out.println("Mari on noor? " + mari.isYoung());
        System.out.println("Mari on noor? " + Human.isThisHumanYoung(mari));

        Person personMari = new Person("44598434535");
        System.out.println("Mari info...");
        System.out.println("Sugu: " + personMari.getGender());
        System.out.println("Sünnikuu: " + personMari.getBirthMonth());
        System.out.println("Sünnikuupäev " + personMari.getBirthDayOfMonth());
        System.out.println("Millal sündinud (aasta)? " + personMari.getBirthYear());


    }
}
