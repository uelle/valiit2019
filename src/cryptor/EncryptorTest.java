package cryptor;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class EncryptorTest {

    private List<String> alphabet;

    @BeforeEach
    public void setup() {
        // String "J, K" --> Map J -> K
        // String "A, E" --> Map A -> E
        this.alphabet = Arrays.asList("J, K", "A, E");
    }

    @AfterEach
    public void teadDown() {

    }

    @Test // peab olema sõltumatu test, mitte olema seotud mõne eelneva testi läbimisega (mitu testi järjest)
    public void testInitAlphabet() {
        // "J, K" --> J -> K
        // "A, E" --> A -> E
        List<String> alphabet = Arrays.asList("J, K", "A, E");
        Encryptor encryptor = new Encryptor();
        encryptor.initAlphabet(alphabet);
        Assertions.assertTrue(encryptor.cryptoMap.get("J").equals("K"));
        Assertions.assertTrue(encryptor.cryptoMap.get("A").equals("E"));

        // encryptor.cryptoMap.get("3")

    }
}



