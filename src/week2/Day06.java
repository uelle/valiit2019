package week2;

import java.math.BigInteger;

public class Day06 {
    public static void main(String[] args) {

        double d1 = 456.78;
        float f1 = 456.78F;

        String t1 = "test";
        char[] ca1 = new char[]{'t', 'e', 's', 't'};
        boolean b1 = true;
        boolean b2 = 5 > 3 && 2 < 1001 || 7 == 8; // True or false --> TRUE
        int[] ia1 = {5, 91, 304, 405};

        double[] da1 = new double[3];
        da1[0] = 56.7;
        da1[1] = 45.8;
        da1[2] = 91.2;

        char charA1 = 'a';
        short charA2 = 'a';
        String charA3 = "a";
        String[] a1 = {
                "see on esimene väärtus",
                "67",
                "58.92"

        };

        //BigDecimal - komakohaga arvud, BigInteger - täisarvud.

        BigInteger bigNumber = new BigInteger("987987987878787878787878787");
        bigNumber = bigNumber.multiply(new BigInteger("2"));


    }
    // Rekursiivne funktsioon

    public static void printNumbersDesc(int number) {
        System.out.println(number);
        if (number < 1) {
            printNumbersDesc(--number);

        }

    }

}










