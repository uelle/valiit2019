package pank;

public class Account {

    private String firstName;
    private String lastName;
    private String accountNumber;
    private double balance;


    public Account(String firstName, String lastName, String accountNumber, double balance) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountNumber = accountNumber;
        this.balance = balance;

    }

    public String getFirstName() {
        return firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public String getAccountNumber() {
        return accountNumber;
    }

    public double getBalance() {
        return balance;

    }
    public void setBalance(double balance) {
        this.balance = balance;

    }

}










