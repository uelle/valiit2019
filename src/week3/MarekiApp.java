package week3;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Scanner;

public class MarekiApp {

    public static void main(String[] args) throws IOException {
        readFromFile1();
        readFromFile2();
        readFromFile3();
        readFromFile4();

        writeToFile1();
        appendToFile1();
        writeToFile2();
        writeToFile3();
        writeToFile4();
    }

    // Reading from file...

    // Option1: BufferedReader --> loeme sisse ridu
    private static void readFromFile1() throws IOException {
        BufferedReader bufferedReader = null;
        try {
            File file = new File("resources/test1.txt");
            bufferedReader = new BufferedReader(new FileReader(file));
            String st;
            System.out.println("Sample 1: BufferedReader:");
            while ((st = bufferedReader.readLine()) != null) { // loeme iga rea läbi muutujasse st
                System.out.println(st);
            }
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close(); // viide failile vabastatakse ja see lastakse lahti
            }
        }
    }

    // Option 2: FileReader --> loeme sisse baite
    private static void readFromFile2() throws IOException {
        FileReader fr = null;
        try {
            fr = new FileReader("resources/test1.txt");
            int i;
            System.out.println("Sample 2: FileReader:");
            while ((i = fr.read()) != -1) {
                System.out.print((char) i);
            }
            System.out.println();
        } finally {
            if (fr != null) {
                fr.close();
            }
        }
    }

    // Option 3: Scanner --> lugeda sisse faile, lugeda sisse konsoolilt, opereerida Stringidega
    private static void readFromFile3() throws FileNotFoundException {
        ClassLoader classLoader = MarekiApp.class.getClassLoader();
        File file = new File("resources/test1.txt");

        // Option 3.1: simple reading
        Scanner sc = new Scanner(file);
        System.out.println("Sample 3.1: Scanner / simple reading");
        while (sc.hasNextLine()) {
            System.out.println(sc.nextLine());
        }

        // Option 3.2: delimiter --> splitib teksti katki, nt alates Rida: (vt näide all)
        sc = new Scanner(file);
        sc.useDelimiter("Rida: ");
        System.out.println("Sample 3.1: Scanner / delimiter");
        while (sc.hasNext()) {
            System.out.print(sc.next());
        }
        System.out.println();
    }

    // Option 4: Read file as list --> väikseste failide puhul tasuks seda eelistada
    private static void readFromFile4() throws IOException {
        Path path = Paths.get("resources/test1.txt");
        List<String> lines = Files.readAllLines(path);
        System.out.println("Sample 4: read file as list");
        for (String line : lines) {
            System.out.println(line);
        }
    }

    // Writing to file ...

    // Option 1: BufferedWriter
    private static void writeToFile1() throws IOException {
        String str = "Hello";
        BufferedWriter writer = new BufferedWriter(new FileWriter("resources/file1.txt"));
        writer.write(str);
        writer.close();
    }

    private static void appendToFile1() throws IOException {
        String str = "Appended Hello";
        BufferedWriter writer = new BufferedWriter(new FileWriter("resources/file1.txt", true));
        writer.write(str);
        writer.close();
    }

    // Option 2: PrintWriter
    private static void writeToFile2() throws IOException {
        FileWriter fileWriter = new FileWriter("resources/file2.txt");
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println("This is a string.");
        printWriter.printf("My favourite phone manufacturer is %s and I am ready to pay %d $ for an iPhone.", "Apple", 1000);
        printWriter.close();
    }

    // Option 3: FileOutputStream (for binary data)
    private static void writeToFile3() throws IOException {
        String str = "Hello";
        FileOutputStream outputStream = new FileOutputStream("resources/file3.txt");
        byte[] strToBytes = str.getBytes();
        outputStream.write(strToBytes);
        outputStream.close();
    }

    // Option 4: write to file using Java 7
    private static void writeToFile4() throws IOException {
        Path filePath = Paths.get("resources/file4.txt");
        byte[] content = "This is a random text.".getBytes();
        Files.write(filePath, content, StandardOpenOption.CREATE, StandardOpenOption.APPEND); // append -liidab juurde, write - kirjutab üle
    }

}



