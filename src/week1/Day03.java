package week1;

import java.util.Scanner;

public class Day03 {

    public static final double VAT_RATE = 0.1; // SCREAMING_SNAKE_CASE
    public static void main(String[] args) {

        // Harjutus 3
//        Scanner poemInput = new Scanner(System.in);
//        System.out.println("Kirjutas üks ilus luuletus: \n");
//        String poem = poemInput.nextLine(); // Jääb ootama kkasutaja poolset sisendit
//        System.out.println("Luuletus oli selline:\n");
//        System.out.println(poem);

        // Harjutus 4

// Vajalik mälu efektiivse kasutamise jaoks. Mõistlik suurte stringide kokkupanemisel.
//
//        StringBuilder myStringBuilder = new StringBuilder();
//        myStringBuilder.append("Marek, ");
//        myStringBuilder.append("Rein, ");
//        myStringBuilder.append("Liina, ");
//        String allTheNames = myStringBuilder.toString();
//        System.out.println(allTheNames);



        // Harjutus 2

//        String myText = "Tallinn, Tartu, Valga, Rapla";
//        Scanner scanner = new Scanner(myText);
//        scanner.useDelimiter(", ");
//
//        while(scanner.hasNext()) {
//            System.out.println(scanner.next());
//        }

        // Harjutus 1

//        String text1 = "Tere";
//        String text2 = "3";
//
//       Integer num1 = Integer.parseInt(text2);
//       // String text3 = num1.toString();
//        String text3 = String.valueOf(num1);
//
//        int myBigNumber = 777;
//        byte mySmallNumber = (byte) myBigNumber;
//        System.out.println(mySmallNumber);
    }
}